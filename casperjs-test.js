casper.test.begin('Fetch, add and remove image tests', 3, function suite(test) {
    casper.start('http://localhost:8000', function() {
      casper.test.info('Waiting 1 second before finding uploaded images...');
      this.wait(1000, function() {
        test.assertExists("img.uploaded-image", "Found uploaded images");

        this.click("img.uploaded-image");
        casper.test.info('Click image to add');

        test.assertElementCount('#canvas-block .added-image', 1, "1 added image found");
     });
    })
    casper.then(function() {
      this.click("#canvas-block .added-image");
      casper.test.info('Click image to delete...');
      this.click("button#deleteelement")
      casper.test.info('Click delete button...');
      test.assertElementCount('#canvas-block .added-image', 0, "0 added image found");
    })
    casper.run(function() {
        test.done();
    });
});


casper.test.begin('Add text working', 3, function suite(test) {
    casper.start('http://localhost:8000', function() {
      test.assertExists('input#textinput');
      test.assertExists('button#addText');

      this.fillSelectors('.assets', {
         'input#textinput':    'I am watching you',
      }, true);
      this.click('button#addText');
      test.assertExists('#canvas-block .added-text', 'Added text exists');
    });

    casper.run(function() {
        test.done();
    });
});
