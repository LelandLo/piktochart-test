## Approach
I decided to go with a minimal approach for this test after looking at the instructions because it says to avoid as much libraries as possible.

I only use jQuery in doing the test. I have been coding with React for the past year and wanted to refresh my jQuery and I thought it would be an interesting approach for this assignment.

## Reactive Rendering
I use render functions (renderCanvas() and renderUploadedImages()) so that the data is flowed in a reactive, unidirectional way. JQuery bindings were used to enable each function, and every time there is a change to the canvas or the uploaded images, the corresponding function is called again. The state of the application is stored in a simple javascript object (elementsAdded) that is used to render the canvas.

## Local Storage Auto Save
To enable objects to be repopulated, I simply stringify the elementsAdded object and parse it upon every loading of the application. I decided to make this an autosave every time the canvas is modified as oppose to a manual save button. To reset, localStorage.clear() can be called in the console. While I did not include a reset button in the app, to do so would be trivial given the way I have set it up.

## CasperJS tests
I created a simple test file called casperjs-test.js. To test, simply install PhantomJS and CasperJS, then run 'casperjs test casperjs-test.js'. I tested the following things:

1. Ajax fetching images from the server
2. Adding images to the canvas
3. Deleting images from the canvas
4. Adding text to the canvas

## Thanks
Finally, thank you for the opportunity to do this test. It was a fun refresher for jQuery for me, something that I have not used in a while. Should you find my work satisfactory, please do contact me. I hope to be able to work with you soon!
