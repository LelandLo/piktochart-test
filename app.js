var elementsAdded = {};
var selectedTarget = null;

function enableMovingAndSelecting() {
  var isDragging = false;
  var target = null;
  $("#canvas-block")
  .mousedown(function(e) {
    isDragging = true;
    e.preventDefault();
    if ($(e.target).attr('canvaskey')) {
      target = $(e.target)

      //calculate offset of cursor within element to give smooth moving experience
      withinElementOffsetX = e.pageX - target.offset().left;
      withinElementOffsetY = e.pageY- target.offset().top;
      selectedTarget = $(e.target).attr('canvaskey');
      renderCanvas()
    } else {
      selectedTarget = null;
      renderCanvas();
    }
  })
  .mousemove(function(e) {
    if (isDragging = true && target) {
      elementsAdded[target.attr('canvaskey')].x = e.pageX - $("#canvas-block").offset().left - withinElementOffsetX;
      elementsAdded[target.attr('canvaskey')].y = e.pageY - $("#canvas-block").offset().top - withinElementOffsetY;
      renderCanvas();
    }
   })
  .mouseup(function(e) {
    target = null;
    isDragging = false;
    renderCanvas();
  });
}

function fetchUploads() {
  $.ajax({
    url: 'http://localhost:8000/images',
    type: 'GET',
    success: function(data){
      images = [];
      for (var i = 0; i < data.length; ++i) {
        images.push(data[i])
      }
      renderUploadedImages();
    }
  });
}

function enableImageUpload() {
  $("#submit").click(function(){
    var input = document.getElementById('fileinput')
    var files = $('#fileinput').prop('files');
    if (!files) {
      alert("This browser doesn't seem to support the `files` property of file inputs.");
      return;
    }
    else if (!files[0]) {
      alert("Please select a file before clicking 'Upload'");
      return;
    }
    var formData = new FormData();
    formData.append('upload', files[0])
    $.ajax({
      url: 'http://localhost:8000/uploads',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      type: 'POST',
      success: function(data){
        $("#fileinput").val("");
        fetchUploads();
      }
    });
  });
}
function enableAddText() {
  $("#addText").click(function() {
    var rand = Math.random();
    var text = $("#textinput").val();
    if (text == "" || !text) {
      alert('Please enter text to add to canvas.');
      return;
    }
    elementsAdded[rand] = {
      text: text,
      x: 100,
      y: 100,
    }
    $("#textinput").val("");
    renderCanvas();
  });
}
function enableAddImage() {
  $("#uploaded-images .uploaded-image").click(function(e) {
    var rand = Math.random();
    var rand = Math.random();
    var src = e.target.src;
    elementsAdded[rand] = {
      imageUrl: src,
      x: 100,
      y: 100,
    }
    renderCanvas();
  });
}
function enableDeleteElement() {
  $("#deleteelement").click(function() {
    var toDelete = selectedTarget;
    selectedTarget = null;
    delete elementsAdded[toDelete]
    renderCanvas();
  });
}

function renderUploadedImages() {
  $("#uploaded-images").empty();
  for (var j = 0; j < images.length; ++j) {
    $( "#uploaded-images" ).append( '<img width="70" height="70" style="margin: 5px" src="' + images[j] + '" class="img-rounded uploaded-image" />');
  }
  enableAddImage();
}

function renderCanvas() {
  $("#canvas-block").empty();
  for(var key in elementsAdded) {
    var element;
    if (elementsAdded[key].text) {
      element = $("<div class='added-text'></div>").text(elementsAdded[key].text);
      element.attr('canvaskey', key)
      element.css('font-size', '2em');
      element.css('left', elementsAdded[key].x);
      element.css('top', elementsAdded[key].y);
      element.css('position', 'absolute');
      if (key == selectedTarget) {
        element.css('border', '1px dashed grey');
      } else {
        element.css('padding', '1px');
      }
    } else {
      element = $("<img class='added-image' src=" + elementsAdded[key].imageUrl + " />")
      element.attr('canvaskey', key)
      element.css('font-size', '2em');
      element.css('left', elementsAdded[key].x);
      element.css('top', elementsAdded[key].y);
      element.css('position', 'absolute');
      if (key == selectedTarget) {
        element.css('border', '1px dashed grey');
      } else {
        element.css('padding', '1px');
      }
    }
    $("#canvas-block").append(element)
  }
  if (selectedTarget) {
    $('#deleteelement').addClass("visible");
  } else {
    $('#deleteelement').removeClass("visible");
  }
  localStorage.setItem('elementsAdded', JSON.stringify(elementsAdded));
}

$(document).ready(function(){
  //if there are added elements in localStorage, load them and render
  if (localStorage.getItem('elementsAdded')) {
    elementsAdded = JSON.parse(localStorage.getItem('elementsAdded'));
    renderCanvas()
  }
  fetchUploads();
  enableImageUpload();
  enableAddText();
  enableMovingAndSelecting();
  enableDeleteElement();
});
